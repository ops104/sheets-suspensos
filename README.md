# Introdução

Este código tem a finalidade de automatizar temporariamente o  processo do Customer Success onde é verificado na planilha cobrar na página do mês atual quais são os clientes que foram suspensos ou que pediram reembolso.Após a verificação os dados dos clientes suspensos/reembolso são passados para uma outra página chamada suspensos que serve para controle das métricas de suspensões.

# Requisitos

arquivo acesso.json disponibilizado pelo google cloud
arquivo .env com o id da planilha

# Dados que são capturados na cobrar

E-mail que está na coluna A
Razão Social que está na coluna B
CNPJ que está na  coluna C
Pagamento que está na coluna D
Início que está na coluna E
Ano que está na coluna F
Plano que está na coluna G
Valor que está na coluna H
Situação que está na coluna K

# Horário de funcionamento
8 horas da manhã
11 horas da manhã
14 horas da tarde
17 horas da tarde
18 horas da tarde
o script verifica se está no horário de funcionamento a cada 30 minutos

# Como funciona

O agendador verifica se está no horário de funcionamento e caso esteja ele executa o job.

O job é um loop com duas entradas Suspensos e reembolso, Na primeira execução ele procura os clientes que estão suspensos na página do mês atual e compara se existe esse mesmo registro na página suspensos e que seja do mesmo mês caso ele não exista ele insere o registro na próxima linha vazia.Após verificar todos os registros suspensos ele troca o parâmetro para reembolso.

# Funções

procurar_status: Recebe dois parâmetros(página que vai trabalhar e uma string com o status que deseja procurar),procura pelas células que tenham o status igual ao status passado no parâmetro e após isso ele captura apenas o index das linhas em uma lista.

capturarPrimeiraLinhaMes: Recebe dois parâmetros(página que vai trabalhar e o mês que deseja procurar) ele irá tentar procurar se existe algum registro com o mês atual caso exista ele vai guardar a posição primeira linha onde o mês atual aparece,caso não ele insere o registro do mês atual.

suspensosDoMes: Recebe 2 parâmetros(primeira linha para procurar e lista de index de linhas) adiciona todos os clientes que estão suspensos/reebolso já inclusos na página suspensos a partir da primeira linha em que o mês atual aparece.

capturarValoresInRows:Recebe 2 parâmetros(lista de index de linhas , página que vai trabalhar)através do index das linhas ele captura os registros inseridos em cada linha e transforma em um dataframe.

df_diff: é a diferença entre 2 dataframes , é armazenado apenas os registros que que fazem parte apenas de 1 dataframe.Isto é ele compara se o registro da página do mês atual já esta incluso do data frame da página suspensos.

dataFrameFilter: Função apenas para inserir as colunas certas na página da suspenso

set_with_dataframe: função da biblioteca gspread_dataframe para inserir dados de um data frame em um planilha


