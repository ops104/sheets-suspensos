from datetime import datetime, date, time, timedelta, timezone
import locale
import pandas as pd



def extractMoth():
    locale.setlocale(locale.LC_ALL, 'pt_BR')
    data = datetime.today()
    data = data.strftime('%b')
    data = data.upper()
    return data


def extractYear():
    locale.setlocale(locale.LC_ALL, 'pt_BR')
    data = datetime.today()
    data = data.strftime('%y')
    return data


def planilhaMesAnoAtual():
    mes = extractMoth()
    ano = extractYear()
    planilhaMesAnoAtual = mes + ano
    planilhaMesAnoAtual = planilhaMesAnoAtual.upper()
    return planilhaMesAnoAtual


def suspensosDoMes(primeiraLinha, IndexSuspenso):
    setMesAtual = {0}
    for item in IndexSuspenso:
        if item >= primeiraLinha:
            setMesAtual.add(item)
    setMesAtual.remove(0)
    return setMesAtual


def consertaValor(dataFrame):
    dataFrame['VALOR'] = dataFrame['VALOR'].str.replace('$', '')
    return dataFrame
