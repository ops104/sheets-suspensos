import pandas as pd


def next_rowEmpty(worksheet):
    proximaCell = len(worksheet.col_values(1)) + 1
    return proximaCell


def procurar_status(worksheet, status):
    index = []
    cell = worksheet.findall(status)
    for row in cell:
        index.append(row.row)
    return index


def capturarValoresInRows(indexLinhas, worksheet):
    listaDeRows = []
    try:
        for rows in indexLinhas:
            lista = worksheet.get_all_values()[rows-1]
            char = {'E-mail': lista[0],
                    'Razão Social': lista[1],
                    'CNPJ': lista[2],
                    'INICIO': lista[4],
                    'ANO': lista[5],
                    'PLANO': lista[6],
                    'VALOR': lista[7],
                    'Situação': lista[10]
                    }
            listaDeRows.append(char)
        return listaDeRows
    except Exception as e:
        print(e)
    return listaDeRows


def capturarValoresInRowsInclusos(indexLinhas, worksheet):
    listaDeRows = []
    try:
        for rows in indexLinhas:
            lista = worksheet.get_all_values()[rows-1]
            char = {'E-mail': lista[0],
                    'Razão Social': lista[1],
                    'CNPJ': lista[2],
                    'INICIO': lista[3],
                    'ANO': lista[4],
                    'PLANO': lista[5],
                    'VALOR': lista[6],
                    'Situação': lista[9],
                    }
            listaDeRows.append(char)
        return listaDeRows
    except Exception as e:
        print(e)
    return listaDeRows


def capturarPrimeiraLinhaMes(worksheet, mes):
    while True:
        try:
            cell = worksheet.find(mes)
            return cell.row
        except:
            rowEmpty = next_rowEmpty(worksheet)
            worksheet.batch_update(
                [{'range': "K{}".format(rowEmpty), 'values': [[mes]]}])


def comparar_dataframe(df_inclusos, df_paginaMeSuspensos):
    clientes_naoInseridos = []
    tamanhoDF = df_paginaMeSuspensos.shape[0]
    for rows in range(0, tamanhoDF):
        row = df_paginaMeSuspensos.iloc[rows]
        char = {'E-mail': row['E-mail'], 'razao_social': row['Razão Social']}
        try:
            resultado = df_inclusos[(df_inclusos['E-mail'] == char['E-mail']) & (
                df_inclusos['Razão Social'] == char['razao_social'])]
        except:
            resultado = pd.DataFrame()
        if resultado.empty:
            clientes_naoInseridos.append(row)
    dataframe = pd.DataFrame(clientes_naoInseridos)
    return dataframe


def inserirClientesSuspensos(dataframe, worksheet, mes):
    listaClientes = []
    tamanhoDF = dataframe.shape[0]
    for rows in range(0, tamanhoDF):
        row = dataframe.iloc[rows]
        listaClientes = [row['E-mail'], row['Razão Social'], row['CNPJ'], row['INICIO'],
                         row['ANO'], row['PLANO'], row['VALOR'], None, None, row['Situação'], mes]
        next_row = next_rowEmpty(worksheet)  # PEGA A LINHA VAZIA
        data = [{'range': f'A{next_row}:K{next_row}','values': [listaClientes],
                'value_input_option': 'USER_ENTERED'}]
        worksheet.batch_update(data)  # ADICIONA VALOR NA LINHA VAZIA

def dataFrameFilter(dataframe, worksheet, mes):
    dataframeFilter = pd.DataFrame()
    dataframeFilter['E-mail'] = dataframe['E-mail']
    dataframeFilter['Razão Social'] = dataframe['Razão Social']
    dataframeFilter['CNPJ'] = dataframe['CNPJ']
    dataframeFilter['INICIO'] = dataframe['INICIO']
    dataframeFilter['ANO'] = dataframe['ANO']
    dataframeFilter['PLANO'] = dataframe['PLANO']
    dataframeFilter['VALOR'] = dataframe['VALOR']
    dataframeFilter['ID'] = ""
    dataframeFilter['VENDEDOR'] = ""
    dataframeFilter['Situação'] = dataframe['Situação']
    dataframeFilter['Mês'] = mes
    return dataframeFilter



    planilhaCobrar.values_append('SuspensoTESTE', {'valueInputOption': 'RAW'}, {'values': df_values})