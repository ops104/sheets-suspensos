import gspread
from gspread_dataframe import set_with_dataframe
import pandas as pd
import fsheets
import transform
import os
from dotenv import load_dotenv
from pathlib import Path


dotenv_path = Path('.env')
load_dotenv(dotenv_path)

id_planilha = os.environ.get('ID_PLANILHA')
planilhaMesAnoAtual = transform.planilhaMesAnoAtual()
gc = gspread.service_account(filename='acesso.json')
planilhaCobrar = gc.open_by_key(id_planilha)
suspensos = planilhaCobrar.worksheet('Suspensos')
paginaMes = planilhaCobrar.worksheet(planilhaMesAnoAtual)


status = ['SUSPENSO','REEMBOLSO']
for situacao in status:
    paginaMes_indexSuspenso = fsheets.procurar_status(paginaMes, situacao)

    paginaMes_rows = fsheets.capturarValoresInRows(
            paginaMes_indexSuspenso, paginaMes)

    paginaMesDf = pd.DataFrame(paginaMes_rows)

    primeiraLinha = fsheets.capturarPrimeiraLinhaMes(
            suspensos, planilhaMesAnoAtual)

    suspensos_indexSuspenso = fsheets.procurar_status(suspensos, situacao)

    clientesSuspensosInclusos = transform.suspensosDoMes(
            primeiraLinha, suspensos_indexSuspenso)

    clientesSuspensosInclusos_df = pd.DataFrame(
            fsheets.capturarValoresInRowsInclusos(clientesSuspensosInclusos, suspensos))

    df_diff = pd.concat([paginaMesDf, clientesSuspensosInclusos_df]
                            ).drop_duplicates(keep=False)

    if df_diff.empty != True:
        dataFrameToInsert = fsheets.dataFrameFilter(df_diff,suspensos,planilhaMesAnoAtual.upper())
        newRow=fsheets.next_rowEmpty(suspensos)
        set_with_dataframe(suspensos,dataFrameToInsert,row=newRow,include_column_header=False)